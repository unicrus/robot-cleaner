﻿using System;
using System.Collections.Generic;

namespace TheCleaner
{
	public class DumbCleaner : ITestableCleaner
	{
		private readonly HashSet<Coordinates> _visitedLocations = new HashSet<Coordinates>();
		private Coordinates _location;

		public DumbCleaner(int x, int y)
		{
			_location = new Coordinates(x, y);
			_visitedLocations.Add(_location);
		}

		public long CleanedPlaces => _visitedLocations.Count;

		public Coordinates Location => _location;

		public void Navigate(Direction direction, int distance)
		{
			var correction = GetCorrection(direction, distance);
			var newLocation = new Coordinates
			{
				X = _location.X + correction.X,
				Y = _location.Y + correction.Y
			};

			NavigateTo(newLocation);
		}

		public void NavigateTo(Coordinates newLocation)
		{
			int startX, startY, finishX, finishY;
			if (newLocation.X > _location.X)
			{
				startX = _location.X;
				finishX = newLocation.X;
			}
			else
			{
				startX = newLocation.X;
				finishX = _location.X;
			}

			if (newLocation.Y > _location.Y)
			{
				startY = _location.Y;
				finishY = newLocation.Y;
			}
			else
			{
				startY = newLocation.Y;
				finishY = _location.Y;
			}

			if (startX != finishX && startY != finishY)
				throw new NotSupportedException("Supported only navigation in a straight line");

			for (var x = startX; x <= finishX; x++)
			{
				for (var y = startY; y <= finishY; y++)
				{
					_visitedLocations.Add(new Coordinates(x, y));
				}
			}

			_location = newLocation;
		}

		private static Coordinates GetCorrection(Direction direction, int distance)
		{
			var correction = new Coordinates();
			switch (direction)
			{
				case Direction.East:
					correction.X = distance;
					break;
				case Direction.West:
					correction.X = -distance;
					break;
				case Direction.North:
					correction.Y = distance;
					break;
				case Direction.South:
					correction.Y = -distance;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			return correction;
		}
	}
}
