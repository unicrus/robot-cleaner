﻿namespace TheCleaner
{
	public interface ICleaner
	{
		long CleanedPlaces { get; }

		void Navigate(Direction direction, int distance);
	}
}
