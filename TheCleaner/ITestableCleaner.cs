﻿namespace TheCleaner
{
	public interface ITestableCleaner: ICleaner
	{
		Coordinates Location { get; }

		void NavigateTo(Coordinates newLocation);
	}
}
