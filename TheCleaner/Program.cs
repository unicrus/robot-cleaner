﻿using System;

namespace TheCleaner
{
	class Program
	{
		static void Main(string[] args)
		{
			var n = int.Parse(Console.ReadLine());
			var start = Console.ReadLine().Split(' ');
			var x = int.Parse(start[0]);
			var y = int.Parse(start[1]);

			ICleaner cleaner = new DumbCleaner(x, y);

			for (var i = 0; i < n; i++)
			{
				var command = Console.ReadLine().Split(' ');
				Direction direction;
				switch (command[0])
				{
					case "E":
						direction = Direction.East;
						break;
					case "W":
						direction = Direction.West;
						break;
					case "N":
						direction = Direction.North;
						break;
					case "S":
						direction = Direction.South;
						break;
					default:
						throw new NotSupportedException();
				}

				var distance = int.Parse(command[1]);

				cleaner.Navigate(direction, distance);
			}

			Console.WriteLine($"=> Cleaned: {cleaner.CleanedPlaces}");
		}
	}
}
