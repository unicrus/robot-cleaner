﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TheCleaner;

namespace TheCleanerTests
{
	[TestClass]
	public class CleanerNavigationTest
	{
		private ITestableCleaner _cleaner;

		[TestInitialize]
		public void Initialize()
		{
			_cleaner = new DumbCleaner(0, 0);
		}

		[TestMethod]
		public void Test_NavigateTo_GoesToTarget()
		{
			var target = new Coordinates(0, -42);

			_cleaner.NavigateTo(target);

			Assert.AreEqual(target, _cleaner.Location);
		}

		[TestMethod]
		[ExpectedException(typeof(NotSupportedException))]
		public void Test_ComplexNavigation_IsNotSupported()
		{
			var target = new Coordinates(10, -42);

			_cleaner.NavigateTo(target);
		}

		[TestMethod]
		public void Test_NavigateToEastBorder_Successful()
		{
			var eastBorder = new Coordinates(100000, 0);

			_cleaner.NavigateTo(eastBorder);

			Assert.AreEqual(eastBorder, _cleaner.Location);
		}

		[TestMethod]
		public void Test_NavigateToWestBorder_Successful()
		{
			var westBorder = new Coordinates(-100000, 0);

			_cleaner.NavigateTo(westBorder);

			Assert.AreEqual(westBorder, _cleaner.Location);
		}

		[TestMethod]
		public void Test_NavigateToNorthBorder_Successful()
		{
			var northBorder = new Coordinates(0, 100000);

			_cleaner.NavigateTo(northBorder);

			Assert.AreEqual(northBorder, _cleaner.Location);
		}

		[TestMethod]
		public void Test_NavigateToSouthBorder_Successful()
		{
			var southBorder = new Coordinates(0, -100000);

			_cleaner.NavigateTo(southBorder);

			Assert.AreEqual(southBorder, _cleaner.Location);
		}

		[TestMethod]
		public void Test_MovingEast_ChangesLocation()
		{
			_cleaner.Navigate(Direction.East, 42);

			Assert.AreEqual(42, _cleaner.Location.X);
			Assert.AreEqual(0, _cleaner.Location.Y);
		}

		[TestMethod]
		public void Test_MovingWest_ChangesLocation()
		{
			_cleaner.Navigate(Direction.West, 42);

			Assert.AreEqual(-42, _cleaner.Location.X);
			Assert.AreEqual(0, _cleaner.Location.Y);
		}

		[TestMethod]
		public void Test_MovingNorth_ChangesLocation()
		{
			_cleaner.Navigate(Direction.North, 42);

			Assert.AreEqual(0, _cleaner.Location.X);
			Assert.AreEqual(42, _cleaner.Location.Y);
		}

		[TestMethod]
		public void Test_MovingSouth_ChangesLocation()
		{
			_cleaner.Navigate(Direction.South, 42);

			Assert.AreEqual(0, _cleaner.Location.X);
			Assert.AreEqual(-42, _cleaner.Location.Y);
		}

		[TestMethod]
		public void Test_NorthWestSquarePath_ReturnsToStart()
		{
			_cleaner.Navigate(Direction.North, 2);
			_cleaner.Navigate(Direction.West, 2);
			_cleaner.Navigate(Direction.South, 2);
			_cleaner.Navigate(Direction.East, 2);

			Assert.AreEqual(0, _cleaner.Location.X);
			Assert.AreEqual(0, _cleaner.Location.Y);
		}

		[TestMethod]
		public void Test_WestSouthSquarePath_ReturnsToStart()
		{
			_cleaner.Navigate(Direction.West, 2);
			_cleaner.Navigate(Direction.South, 2);
			_cleaner.Navigate(Direction.East, 2);
			_cleaner.Navigate(Direction.North, 2);

			Assert.AreEqual(0, _cleaner.Location.X);
			Assert.AreEqual(0, _cleaner.Location.Y);
		}

		[TestMethod]
		public void Test_SouthEastSquarePath_ReturnsToStart()
		{
			_cleaner.Navigate(Direction.South, 2);
			_cleaner.Navigate(Direction.East, 2);
			_cleaner.Navigate(Direction.North, 2);
			_cleaner.Navigate(Direction.West, 2);

			Assert.AreEqual(0, _cleaner.Location.X);
			Assert.AreEqual(0, _cleaner.Location.Y);
		}

		[TestMethod]
		public void Test_EastNorthSquarePath_ReturnsToStart()
		{
			_cleaner.Navigate(Direction.East, 2);
			_cleaner.Navigate(Direction.North, 2);
			_cleaner.Navigate(Direction.West, 2);
			_cleaner.Navigate(Direction.South, 2);

			Assert.AreEqual(0, _cleaner.Location.X);
			Assert.AreEqual(0, _cleaner.Location.Y);
		}
	}
}
