﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using TheCleaner;

namespace TheCleanerTests
{
	[TestClass]
	public class CleanerReportingTest
	{
		private ITestableCleaner _cleaner;

		[TestInitialize]
		public void Initialize()
		{
			_cleaner = new DumbCleaner(-2, -2);
		}

		[TestMethod]
		public void Test_MovingEast_CountsLocations()
		{
			_cleaner.Navigate(Direction.East, 42);

			Assert.AreEqual(43, _cleaner.CleanedPlaces);
		}

		[TestMethod]
		public void Test_MovingWest_CountsLocations()
		{
			_cleaner.Navigate(Direction.West, 42);

			Assert.AreEqual(43, _cleaner.CleanedPlaces);
		}

		[TestMethod]
		public void Test_MovingNorth_CountsLocations()
		{
			_cleaner.Navigate(Direction.North, 42);

			Assert.AreEqual(43, _cleaner.CleanedPlaces);
		}

		[TestMethod]
		public void Test_MovingSouth_CountsLocations()
		{
			_cleaner.Navigate(Direction.South, 42);

			Assert.AreEqual(43, _cleaner.CleanedPlaces);
		}

		[TestMethod]
		public void Test_NorthWestSquarePath_CountsLocations()
		{
			_cleaner.Navigate(Direction.North, 2);
			_cleaner.Navigate(Direction.West, 2);
			_cleaner.Navigate(Direction.South, 2);
			_cleaner.Navigate(Direction.East, 2);

			Assert.AreEqual(8, _cleaner.CleanedPlaces);
		}

		[TestMethod]
		public void Test_WestSouthSquarePath_CountsLocations()
		{
			_cleaner.Navigate(Direction.West, 2);
			_cleaner.Navigate(Direction.South, 2);
			_cleaner.Navigate(Direction.East, 2);
			_cleaner.Navigate(Direction.North, 3);

			Assert.AreEqual(9, _cleaner.CleanedPlaces);
		}

		[TestMethod]
		public void Test_SouthEastSquarePath_CountsLocations()
		{
			_cleaner.Navigate(Direction.South, 2);
			_cleaner.Navigate(Direction.East, 2);
			_cleaner.Navigate(Direction.North, 2);
			_cleaner.Navigate(Direction.West, 2);
			_cleaner.Navigate(Direction.South, 3);

			Assert.AreEqual(9, _cleaner.CleanedPlaces);
		}

		[TestMethod]
		public void Test_EastNorthSquarePath_CountsLocations()
		{
			_cleaner.Navigate(Direction.East, 2);
			_cleaner.Navigate(Direction.North, 2);
			_cleaner.Navigate(Direction.West, 2);
			_cleaner.Navigate(Direction.South, 4);

			Assert.AreEqual(10, _cleaner.CleanedPlaces);
		}
	}
}
